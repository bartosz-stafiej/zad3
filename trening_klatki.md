---
author: Bartosz Stafiej
title: Trening Kaltki Piersiowej
subtitle: Beamer
date: 27.10.2021
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Budowa Klatki Piersiowej

Mięśnie klatki piersiowej tworzą 3 odrębne zespoły. Ich zadaniem jest
nie tylko poruszanie poszczególnymi częściami ciała, lecz również
ochrona ważnych narządów oraz wspomaganie przy wykonywaniu życiowych
czynności, np. oddychania. Są to:

*   mięśnie powierzchowne - przyczepiające się do kośćca okolicy barku
    i ramienia;

*   mięśnie głębokie - stanowiące właściwe mięśnie ściany klatki
    piersiowej;

*   przepona - tworzącą przegrodę między jamą brzuszną, a jamą klatki
    piersiowej.

## Ruchy jakie wykonuje klatka

Taka budowa sprawia, że klatka wykonuje 3 główne ruchy:

*   wyciskanie nad klatkę piersiową

*   przywodzenie ramion przed klatkę piersiową

*   odwodzenie ramion nad klatkę piersiową




##
\begin{center}
    \huge \color{blue} Przykładowe ćwiczenia klatki piersiowej
\end{center}

##  Przykładowe ćwiczenia klatki piersiowej

Wyciskanie sztangielek leżąc na ławeczce dodatniej. Jest to świetne
ćwiczenie , które rozwinie górę mięśnia piersiowego większego.


![image][1]

##  Przykładowe ćwiczenia klatki piersiowej

Wyciskanie sztangielek leżąc na ławeczce ujemnej. Jest to świetne
ćwiczenie , które rozwinie dół mięśnia piersiowego większego.

![image][2]

##  Przykładowe ćwiczenia klatki piersiowej

Krzyżowanie linek na bramie wyciągu górnego. Ćwiczenie, które rozwija
przede wszystkim dolne i środkowe aktony klatki piersiowej


![image][3]

##  Przykładowe ćwiczenia klatki piersiowej

Przenoszenie sztangielki nad głowę leżąc na ławeczce płaskiej. Ostatni
ruch, który jest wymagany do idelanego treningu klatki piersiowej


![image][4]


  [image]: Rysunek3.jpg
  [1]: Rysunek34.jpg
  [2]: Rysunek33.jpeg
  [3]: Rysunek32.png
  [4]: Rysunek35.jpg